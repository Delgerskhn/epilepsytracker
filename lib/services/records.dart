import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:patient_tracker/models/record.dart';

Future<void> addRecord(Record r) {
  CollectionReference records =
      FirebaseFirestore.instance.collection('records');
  // Call the user's CollectionReference to add a new user
  return records
      .add({
        'datetime': r.date, // John Doe
        'symptom': r.symptom, // Stokes and Sons
      })
      .then((value) => print("User Added"))
      .catchError((error) => print("Failed to add user: $error"));
}

Future<List<Record>> readWeeklyRecords() async {
  var records = await FirebaseFirestore.instance
      .collection('records')
      .where('datetime',
          isGreaterThanOrEqualTo: DateTime.now().add(const Duration(days: -7)))
      .get()
      .then((snapshot) => snapshot.docs.map((doc) {
            return Record(doc['symptom'], doc['datetime']);
          }))
      .catchError((e) {
    print(e);
    throw e;
  });

  return records.toList();
}
