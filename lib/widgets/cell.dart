import 'package:flutter/cupertino.dart';

class Cell extends StatelessWidget {
  final String value;
  const Cell({required this.value});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(value),
      padding: const EdgeInsets.all(3),
    );
  }
}
