import 'package:flutter/material.dart';
import 'package:patient_tracker/models/record.dart';
import 'package:patient_tracker/services/records.dart';
import 'package:patient_tracker/widgets/cell.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (BuildContext context, AsyncSnapshot<List<Record>> snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
          return Text("Something went wrong");
        }

        if (snapshot.hasData && snapshot.data!.isEmpty) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          var rows = [
            const TableRow(children: [
              Cell(value: 'Огноо'),
              Cell(value: 'Шинж тэмдгүүд'),
            ]),
          ];
          rows.addAll(snapshot.data!.map((e) => TableRow(children: [
                Cell(value: e.date.toDate().toIso8601String().split('T')[0]),
                Cell(value: e.symptom)
              ])));
          return Table(
            children: rows,
            border: TableBorder.all(width: 1),
          );
        }

        return Text("loading");
      },
      future: readWeeklyRecords(),
    );
  }
}
