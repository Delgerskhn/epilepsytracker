import 'package:cloud_firestore/cloud_firestore.dart';

class Record {
  final String symptom;
  final Timestamp date;

  Record(this.symptom, this.date);
}
