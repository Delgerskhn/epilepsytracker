import 'package:flutter/material.dart';
import 'package:patient_tracker/main.dart';

class TempPage extends Page {
  TempPage() : super(key: ValueKey('HomePage'));

  @override
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) => Text("Temp"),
    );
  }
}
