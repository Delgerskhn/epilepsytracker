import 'package:flutter/cupertino.dart';
import 'package:patient_tracker/router/homepage%20copy.dart';
import 'package:patient_tracker/router/homepage.dart';

class NavigatorModel extends ChangeNotifier {
  List<Page> _pages = [HomePage()];

  get pages => _pages;

  Future<void> push() => Future.delayed(Duration(seconds: 2)).then((value) {
        _pages = [TempPage()];
        notifyListeners();
      });
}
