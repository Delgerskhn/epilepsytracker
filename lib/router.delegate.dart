import 'dart:io';

import 'package:flutter/material.dart';
import 'package:patient_tracker/router/navigator.model.dart';
import 'package:provider/src/provider.dart';

class IRouterDelegate extends RouterDelegate
    with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  final GlobalKey<NavigatorState> _navigatorKey;

  @override
  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  IRouterDelegate() : _navigatorKey = GlobalKey<NavigatorState>() {}

  // String _selectedColorCode;
  // String get selectedColorCode => _selectedColorCode;
  // set selectedColorCode(String value) {
  //   _selectedColorCode = value;
  //   notifyListeners();
  // }

  @override
  Widget build(BuildContext context) {
    final navigator = context.watch<NavigatorModel>();
    // navigator.push();
    return Navigator(
      key: navigatorKey,
      pages: navigator.pages,
      onPopPage: (route, result) {
        if (!route.didPop(result)) return false;
        // if (selectedShapeBorderType == null) selectedColorCode = null;
        // selectedShapeBorderType = null;
        return true;
      },
    );
  }

  @override
  Future<void> setNewRoutePath(configuration) async {/* Do Nothing */}
}
